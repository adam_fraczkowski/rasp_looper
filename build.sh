cd build
#PACKAGE SERVER
pkg . --output ../dist/main_platform/server
#PACKAGE APP
pkg --output ../dist/main_platform/app app.js
#PACKAGE  LOGS SERVICE
pkg --output ../dist/main_platform/logs_task_service LogstaskService.js
#PACKAGE  CONFIG SERVICE
pkg --output ../dist/main_platform/logs_task_service ConfigtaskService.js
#PACKAGE  SCHEDULE SERVICE
pkg --output ../dist/main_platform/schedule_task_service ScheduletaskService.js
#PACKAGE  UPDATE SERVICE
pkg --output ../dist/main_platform/update_task_service UpdatetaskService.js
cd ..
cp build/looper.db dist/main_platform/looper.db
cp build/splash.html dist/main_platform/splash.html
cp build/stolen.html dist/main_platform/stolen.html
cp build/ecosystem.config.js dist/main_platform/ecosystem.config.js
cp splash_screen.png dist/main_platform/splash_screen.png
cp time_script.sh dist/main_platform/time_script.sh
cp app_check.sh dist/main_platform/app_check.sh
cp screen_script.sh dist/main_platform/screen_script.sh
mkdir dist/main_platform/videos


