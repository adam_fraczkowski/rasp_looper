var crypto = require('crypto');
var fs = require('fs');
var boxService = require('./dropboxService.js');
var password = "NYHvbjhd80578vdJKHfuivfjk";
var sign = crypto.createSign('SHA256');
var argv = require('minimist')(process.argv.slice(2));
var dutils = require('./downloadUtils.js');
function parser_check(str)
{
    try {
        JSON.parse(str);
    } catch(e) {
        console.log("PARSING SCHEDULE ERROR. SCHEDULE MUST BE A VALID JSON FILE");
        process.exit();
    }
}

//var argv = {};
//argv.i = "example_schedule.json";
argv.k = "rasp_private.pem";
argv.t = 'AgvSIiQCDoAAAAAAAAAAzUiSaUmwnfRJe-Amnb8lRpmHj-XAbErYf3siZV-2UBYk';

/*
if(!argv.k || !argv.i || !argv.t)
{
    console.log("Params not provided\n");
    console.log("-k : KEY FILE \n");
    console.log("-i : PATH TO SCHEDULE FILE\n");
    console.log("-t : DROPBOX ACCESS TOKEN\n");
    console.log("EXAMPLE: sign -k keyfile.pem -i example_schedule.json -t EXAMPLEACCESSTOKENTODROPBOX")
    process.exit();
}
*/

var sign_file = fs.readFileSync(argv.i);
var private_key = fs.readFileSync(argv.k);
var access_token = argv.t;

parser_check(sign_file);

sign.update(sign_file);

var result = sign.sign(private_key,'base64');
fs.writeFileSync(argv.i+'.signed',dutils.encrypt(sign_file+'#BEGIN#'+result+'#END#'),'utf8')


boxService.upload_file(argv.i+'.signed',access_token)
.then(function(res){
    console.log("Link to new schedule:"+res);
    console.log("Signature:"+result);
})
.catch(function(error){
    console.log("ERROR DURING UPLOAD TO DROPBOX. Check your API KEY")
});
