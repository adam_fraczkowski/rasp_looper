var crypto = require('crypto');
var fs = require('fs');
var boxService = require('./dropboxService.js');
var password = "NYHvbjhd80578vdJKHfuivfjk";
var sign = crypto.createSign('SHA256');
var argv = require('minimist')(process.argv.slice(2));
var dutils = require('./downloadUtils.js');

let update_link = null;
let file_signature = null

function parser_check(str)
{
    try {
        JSON.parse(str);
    } catch(e) {
        console.log("PARSING SCHEDULE ERROR. SCHEDULE MUST BE A VALID JSON FILE");
        process.exit();
    }
}

var argv = {};
//argv.i = "example_update_zip.zip";
argv.k = "rasp_private.pem";
argv.t = 'AgvSIiQCDoAAAAAAAAAAzUiSaUmwnfRJe-Amnb8lRpmHj-XAbErYf3siZV-2UBYk';


if(!argv.k || !argv.i || !argv.t)
{
    console.log("Params not provided\n");
    console.log("-k : KEY FILE \n");
    console.log("-i : PATH TO FILE\n");
    console.log("-t : DROPBOX ACCESS TOKEN\n");
    console.log("EXAMPLE: sign -k keyfile.pem -i example_schedule.json -t EXAMPLEACCESSTOKENTODROPBOX")
    process.exit();
}



var sign_file = fs.readFileSync(argv.i);
var private_key = fs.readFileSync(argv.k);
var access_token = argv.t;

sign.update(sign_file);

file_signature = sign.sign(private_key,'base64');

boxService.upload_file(argv.i,access_token)
.then(function(res){
  update_link = res;
  let str_obj = {}
  str_obj.url = update_link;
  str_obj.signature = file_signature;
  fs.writeFileSync('update_manifest',dutils.encrypt(JSON.stringify(str_obj)));
  return boxService.upload_file('update_manifest',access_token);
  
})
.then(function(res){
    console.log("UPDATE MANIFEST FILE: "+res);
})
.catch(function(error){
    console.log("ERROR DURING UPLOAD TO DROPBOX. Check your API KEY")
});
