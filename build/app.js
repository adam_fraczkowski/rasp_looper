var fs = require('fs');
var db = require('./dbService.js');
var config = {};
var state = "CHECK_LIST";
var splash_active = false;
var preset_active = false;
var child_proc = require('child_process');  
var path = require('path');

var cnt = 0;
var source_list = [];
var curr_app_proc = null;
var splash_app_proc  = null;
var stolen_app_proc = null;




function init()
{
    return new Promise(function(resolve,reject){
        db.get_config().then(function(result){
            config = result;
            db.log('loop_process','Config loaded',"success");
            resolve();
        })
        .catch(function(error){
            db.log('loop_process','Config not loaded',"error");
            reject();
        })
    })
    
}

function update_list()
{
    return new Promise(function(resolve,reject){
        db.fetch_list()
        .then(function(rows){
            source_list = rows;
            db.log('loop_process','Video list updated from database',"success");
            state = 'CHECK_LIST';
            main();
            resolve(source_list)
        })
        .catch(function(error){
            db.log('loop_process','database list empty. Error during update from database.',"error");
            state="CHECK_LIST";
            main();
            reject();
        })
    }) 
    
}

function closeEvents()
{
    curr_app_proc.on('close',function(code){
        preset_active=false;
        if(source_list.length-1==cnt)
        {
            db.log('loop_process','Video ended. loop restart',"info");
            cnt=0;
            update_list()
            .then(function(){
            })
            .catch(error=>function(){
            })
        }
        else
        {
            db.log('loop_process','Video ended. Start new asset',"info");
            cnt = cnt+1;
            state='CHECK_LIST';
            main();
        }
     });
    
     curr_app_proc.on('error',function(code){
        preset_active=false;
        db.log('loop_process','current process video/browser crashed. go to next asset',"error");
        if(source_list.length-1==cnt)
        {
            cnt=0;
            update_list()
            .then(function(){
               
            })
            .catch(error=>function(error){

            })
        }
        else
        {
            cnt=cnt+1;
            state='CHECK_LIST';
            main();
        }
        
     });
}


function main()
{
    switch(state)
    {   
        case 'CHECK_LIST':
            if(source_list.length>0)
            {
                switch(source_list[cnt].type)
                {
                    case 'video':
                    state='SHOW_VIDEO';
                    main();
                    break;

                    case 'browser':
                    state='SHOW_BROWSER';
                    main();
                    break;
                }
            }
            else
            {
                db.log('loop_process','database list empty. No videos in schedule. Start to show default splash screen',"warning");
                state='DEFAULT_SPLASH';
                main();
                
            }
        break;

        case 'SHOW_VIDEO':
            //curr_app_proc = child_proc.spawn(config.video_command,['-b','-o','local','--no-osd',source_list[cnt].path]); 
            if(preset_active==true) curr_app_proc.kill();
            if(splash_active==true) splash_app_proc.kill();
            var args = config.video_command_params.split(';');
            curr_app_proc = child_proc.spawn(config.video_command,[source_list[cnt].path].concat(args)); 
            db.log('loop_process','video started',"asset",source_list[cnt].id);
            splash_active = false;
            preset_active = true;
            closeEvents();
        break;

        case 'SHOW_BROWSER':
            if(preset_active==true) curr_app_proc.kill();
            if(splash_active==true) splash_app_proc.kill();
            var args = config.browser_command_params.split(';');
            curr_app_proc = child_proc.spawn(config.browser_command,[source_list[cnt].path].concat(args))
            db.log('loop_process','browser started','asset',source_list[cnt].id);
            splash_active = false;
            preset_active = true;
            setTimeout(function(){
                state = 'CHECK_LIST'
                curr_app_proc.kill('SIGINT');
            },parseInt(source_list[cnt].timeout))
            closeEvents();
        break;

        case 'DEFAULT_SPLASH':
            if(splash_active==false)
            {
                var args = config.browser_command_params.split(';');
                splash_app_proc = child_proc.spawn(config.browser_command,[config.splash_file].concat(args));
                db.log('loop_process','splash screen active','info');
                splash_active = true;
                preset_active = false;
                update_list();
            }
            else
            {
                setTimeout(function(){
                    update_list();
                    db.log('loop_process','app timeout reached. Trying to update list',"info");
                },config.app_timeout)
            }
            
        break;
        
        case 'STOLEN':
            var args = config.browser_command_params.split(';');
            stolen_app_proc = child_proc.spawn(config.browser_command,[config.stolen_file].concat(args));
            db.log('loop_process','stolen screen active','info');
        break;
    }
   
}


//START PROGRAM
init()
.then(function(){
    if(config.stolen==0)
    {
        update_list();
    }
    else
    {
        state='STOLEN';
        main();
    }
    


})
.catch(error=>function(error)
{
    source_list=[];
    
})

