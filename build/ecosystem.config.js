module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    // First application
    {
      name      : 'logs_service',
      script    : './logs_task_service',
      log_date_format:'YYYY-MM-DD HH:mm Z',
      interpreter:'none'    
    },
    {
      name      : 'schedule_service',
      script    : './schedule_task_service',
      log_date_format:'YYYY-MM-DD HH:mm Z',
      interpreter:'none'
    },
    {
      name      : 'update_service',
      script    : './update_task_service',
      log_date_format:'YYYY-MM-DD HH:mm Z',
      interpreter:'none'
    },
    {
      name      : 'server',
      script    : './server',
      log_date_format:'YYYY-MM-DD HH:mm Z',
      interpreter:'none' 
    },
    {
      name      : 'config_service',
      script    : './config_task_service',
      log_date_format:'YYYY-MM-DD HH:mm Z',
      interpreter:'none' 
    }
  ]


};
