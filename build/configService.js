var dutils = require('./downloadUtils.js');
var db = require('./dbService.js');
var fs = require('fs');

var new_signature= null;
var config_url = null;
var schedule_string = null;
var config_file_global = null;

function config_download_task()
{
    db.get_config_param('config_url')
    .then(cfg_url=>{
        config_url = cfg_url;
        return dutils.request_get(cfg_url);
    })
    .then((res)=>{
        res = dutils.decrypt(res.body);
        console.log("OK");
        config_string=res;
        start_signature = res.indexOf("#BEGIN#");
        var stop_signature = res.indexOf("#END#");
        new_signature = res.substring(start_signature+7,stop_signature);
    })
    .then(()=>db.get_config_param('config_signature'))
    .then(actual_sig=>{
        if(actual_sig==new_signature)
        {
                    
            db.log('config','config is actual. Terminating',"info");
            process.exit()
        }
    })
   .then(()=>dutils.save_schedule(config_string,'tmp_config.json.crypto'))
   .then(()=>dutils.verify_file('tmp_config.json.crypto'))
   .then((res)=>{
        return new Promise(function(resolve,reject){
            db.log('config','new config verified. Try to parse content',"success");
            fs.readFile('tmp_config.json.crypto',function(error,data){
                if(error)
                {
                    db.log('config','Cannot parse config','error');
                    reject();
                }
                else
                {
                    data = data.toString();
                    var config_file = JSON.parse(data.slice(0,start_signature));  
                    if(typeof config_file=="object")
                    {
                        config_file_global = config_file;  
                        resolve(config_file);
                    } 
                    else
                    {
                            db.log('config','Cannot parse config',"error");
                            reject();
                    }
                }
            })
        })

   })
   .then((config_file)=>db.set_config(config_file))
   .then((config_file)=>db.set_config_param("config_signature",new_signature))
   .then(function(){
       process.exit();
       return new Promise();
   })
   .catch(function(error){
        db.log("config","Error During config update. Terminating:::"+error,"error");
        process.exit();
    })
        
}


module.exports = {
    config_download_task:config_download_task
}