let db = require('./dbService.js');
let dutils = require('./downloadUtils.js');
let crypto = require('crypto');
let fs = require('fs');
let AdmZip = require('adm-zip');

let config = null;
let new_signature = null;
let update_file = null;
let encoded = null;
let start_signature=0;
let file_name = null;

function update_service_task()
{

    db.get_config()
    .then(function(config_res){
        config = config_res;
        db.log("software_update","Starting software update process","info");
        return dutils.request_get(config.update_url);
    })
    .then(function(decrypted){
        update_file = JSON.parse(dutils.decrypt(decrypted.body));
        new_signature = update_file.signature;
    })
    .then(function(){
        if(new_signature==config.update_signature)
        {
            db.log("software_update","No software update detected. Terminating","info");
            process.exit();
        }
        else
        {
            db.log("software_update","New software detected. Downloading package from url","info");
            return dutils.download_update(update_file.url);
        }
    })
    .then(function(filename){
        db.log("software_update","New software downloaded. Verify package..","success")
        return dutils.verify_update(filename,new_signature);
    })
    .then(function(fname)
    {
        file_name = fname;
        db.log("software_update","Software verified. Install new software","success");
        return new Promise(function(resolve,reject){
            var zip = new AdmZip(fname);
            zip.extractAllTo(/*target path*/".", /*overwrite*/true);
            resolve();
        })

    })
    .then(function(){
        db.log("software_update","Software updated to new version. Saving signature to db and terminate","success");
        db.set_config_param('update_signature',new_signature);
        fs.unlinkSync(file_name);
        process.exit();
    })
    .catch(function(error){
        db.log("software_update","Error during software update. Terminating","error");
        console.log(error);
        process.exit();
    })
}

module.exports = {
    update_service_task:update_service_task
}