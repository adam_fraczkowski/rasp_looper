var express = require('express');
var db = require('./dbService.js');
var bodyParser = require('body-parser');
var path = require('path');
var basicAuth = require('basic-auth');
var config = {}
var app = express();
var port = "8080";
db.get_config().then(function(result){
    config = result;
    db.log('loop_process','Config loaded',"success");
    //resolve();
    port = config.port
    app.listen(port, () => console.log('Express App: PORT '+port))
    
})
.catch(function(error){
    db.log('loop_process','Config not loaded',"error");
    console.log("DDD");
    app.listen(port, () => console.log('Express App: PORT '+port))
})

process.on('unhandledRejection', error => {
    // Will print "unhandledRejection err is not defined"
    process.exit();
  });



var username = "pi"
var password = "rasp2016$"

//middleware
app.use(function (req,res,next)
{
    var user = basicAuth(req);
    if(!user || user.name !== username || user.pass !== password)
    {
        res.setHeader('WWW-Authenticate', 'Basic realm="MyRealmName"');
        res.status(401).send("Unauthorized");
    }
    else
    {
        next();
    }
})


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));



app.use(function(err, req, res, next) {
    //console.log(err);
    process.exit();
  });




  //ROUTES

  //get logs from database
  app.get('/get_logs',function(req,res,next){
    if(!req.query.limit || !req.query.offset)
    {
        req.query.limit = undefined;
        req.query.offset = undefined;
    }
    db.get_logs(req.query.limit,req.query.offset)
    .then(function(result){
        res.status(200).send(result);
    })
    .catch(function(error){
        res.status(500).send(error);
    });
  });

  
  app.get('/get_schedule',function(req,res,next){
    db.get_schedule()
    .then(function(result){
        res.status(200).send(result);
    })
    .catch(function(error)
    {
        res.status(500).send(error);
    })
  });

app.get('/get_config',function(req,res,next){
    db.get_config().then(function(result){
        res.status(200).send(result);
    })
    .catch(function(error){
        res.status(500).send(error);
    })
});

app.post('/set_config_param',function(req,res,next){
    if(typeof req.body.param=="undefined" || typeof req.body.value=="undefined")
    {
        res.status(400).send("NoParamsProvided");
    }
    else
    {
        db.set_config_param(req.body.param,req.body.value)
        .then(function(resp){
            res.status(200).send("OK");
        })
    }
});

app.post('/generate_schedule',function(req,res,next){

});

app.get('/',function(req,res,next){
    res.sendFile(path.join(__dirname, 'public/index.html'));
});

