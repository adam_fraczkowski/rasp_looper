var dblite = require('dblite');
db = dblite('./looper.db');
var fs = require('fs');
var config = {};
var request = require('request');
var moment = require('moment');

get_config().then(function(){})

function isNumber(n) 
{
    return !isNaN(parseFloat(n)) && isFinite(n);
}


  function get_logs(limit,offset)
  {
      return new Promise(function(resolve,reject){
          if(limit && isNumber(limit) && offset && isNumber(offset))
          {
              db.query("SELECT * FROM logs ORDER BY created DESC LIMIT ? OFFSET ?",[limit,offset],function(error,rows){
                  if(error)
                  {
                      reject(error);
                  }
                  else
                  {
                      resolve(rows);
                  }
              })
          }
          else
          {
              db.query("SELECT * FROM logs ORDER BY created DESC",function(error,rows){
                  if(error)
                  {
                      reject(error);
                  }
                  else
                  {
                      resolve(rows);
                  }
              })
  
          }
      });
  }


function log(log_type,log_text,status,video_id)
{
    db.query("INSERT INTO logs (machine_created,log_type,m_id,text,status,video_id,created) VALUES (?,?,?,?,?,?,?)",[config.machine_created,log_type,config.m_id,log_text,status,video_id,moment().unix()]);
    //fs.appendFileSync('log.log',new Date()+'#'+log_type+'#'+log_text+'\n');
    console.log(log_text);
}

function get_config()
{
    return new Promise(function(resolve,reject){
        db.query('SELECT * FROM configs',function(error,rows){
            if(error)
            {
                reject(error);
            }
            else
            {
                for(var i=0;i<rows.length;i++)
                {
                    
                    config[rows[i][1]] = rows[i][2];
                }
                resolve(config);
            }
        })
    })
}

function set_config(config_obj)
{
    return new Promise(function(resolve,reject){
        db.query("BEGIN TRANSACTION");
        db.query("DELETE FROM configs");
        for(var k in config_obj)
        {
            var vals_array = [k,config_obj[k]];
            db.query("INSERT INTO configs (name,value) VALUES (?,?)",vals_array);
        }
        db.query("COMMIT",function(error,response){
            if(error)
            {
                reject();
            }
            else
            {
                resolve();
            }
        });
    });
}

function set_config_param(param,value)
{
    return new Promise(function(resolve,reject){
        
        db.query("UPDATE configs SET value=? WHERE name=?",[value,param],function(error,res){
            if(error)
            {
                reject();
            }
            else
            {
                resolve();
            }
        })
    });
}



function fetch_list()
{
var list = []; 
return new Promise(function(resolve,reject){
    db.query("SELECT video_id,local_path,type,timeout FROM videos WHERE strftime('%s', 'now') BETWEEN date_from AND date_to AND status='active'",function(error,rows){
        if(error)
        {
            reject(error);
            return;
        }
        else
        {
           for(var i=0; i<rows.length;i++)
           {
               list.push({id:rows[i][0],path:rows[i][1],type:rows[i][2],timeout:rows[i][3]});
           } 
           resolve(list);
           return;
        }
    })
})

}

function get_schedule()
{
    return new Promise(function(resolve,reject){
        db.query("SELECT * FROM videos ORDER BY created DESC",function(error,rows){
            if(error)
            {
                reject(error);
            }
            else
            {
                resolve(rows);
            }
        })
    });
}

function set_schedule(schedule_obj)
{
    return new Promise(function(resolve,reject){
        db.query("BEGIN TRANSACTION");
        db.query("DELETE FROM videos");
        for(var k=0; k<schedule_obj.length;k++)
        {
            var vals_array = ["active",schedule_obj[k].local_path,schedule_obj[k].date_from,schedule_obj[k].date_to,schedule_obj[k].video_id,schedule_obj[k].type,schedule_obj[k].timeout,schedule_obj[k].download_content];
            db.query("INSERT INTO videos (status,local_path,date_from,date_to,video_id,type,timeout,download_content) VALUES (?,?,?,?,?,?,?,?)",vals_array);
        }
        db.query("COMMIT",function(error,response){
            if(error)
            {
                reject();
            }
            else
            {
                resolve();
            }
        });
    });
}


function get_config_param(param)
{
    return new Promise(function(resolve,reject){
        db.query("SELECT value FROM configs WHERE name=?",[param],function(error,rows){
            if(error)
            {
                reject();
            }
            else
            {
                resolve(rows[0][0]);
            }
        });
    })
}

function delete_logs()
{
    return new Promise(function(resolve,reject){
        db.query("DELETE FROM logs WHERE created > ?",[config.logs_timestamp],function(error,rows){
            if(error)
            {
                reject("cannot delete logs during cleaning procedure");
            }
            else
            {
                resolve();
            }
        })
    })
}

function post_logs()
{
    return new Promise(function(resolve,reject){
        db.query("SELECT * FROM logs WHERE created > '?'",[config.logs_timestamp],function(error,rows){
            if(error)
            {
                reject("CANNOT FETCH LOGS FROM DB");
            }
            else
            {
                resolve(rows)
            }
        })
    })
}


module.exports = {
    log:log,
    fetch_list:fetch_list,
    get_logs:get_logs,
    get_schedule:get_schedule,
    set_schedule:set_schedule,
    get_config:get_config,
    get_config_param:get_config_param,
    set_config_param:set_config_param,
    delete_logs:delete_logs,
    post_logs:post_logs,
    set_config:set_config
}