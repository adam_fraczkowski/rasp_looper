var db = require('./dbService.js');
var logs_post_url = '';
var os = require('os');
var request = require('request');
var moment = require('moment');

//change dates to timestamp

function logs_post_task()
{
    var single_json={}
    single_json.summary=[]
    single_json.config=[]
    single_json.logs=[]
    var summary = {}


    summary.timestamp = moment().unix();
    summary.arch = os.arch();
    summary.hostname = os.hostname();
    summary.network = os.networkInterfaces();
    summary.load_summary = os.loadavg();
    summary.uptime = os.uptime(); 
    //timestamp
    db.post_logs()
    .then(function(logs_rows){
        single_json.logs = logs_rows;
        return db.get_config()
    })
    .then(function(config_rows){
        single_json.summary=summary;
        single_json.config=config_rows
    })
    .then(()=>db.get_config_param('logs_push_url'))
    .then(function(url){
        return new Promise(function(resolve,reject){
            request({
                method:'post',
                url:url,
                headers:{'Content-Type':'application/json'},
                body:single_json,
                json:true
            },function(error,res){
                if(error)
                {
                    db.log('logs_push',"data cannot send to external server","error");
                    reject(error);
                }
                else
                {
                    db.log('logs_push',"data send to external server","success");
                    resolve();
                }
            })
        })
    })
    .then(()=>db.delete_logs())
    .then(()=>db.set_config_param("logs_timestamp",moment().unix()))
    .then(()=>db.set_config_param("summary",JSON.stringify(summary)))
    .then(function(){
        db.log('logs_push',"Logs process successful. Terminating","success");
        process.exit();
    })
    .catch(function(error){
        db.log("logs_push","cannot send logs. Error during process","error");
        process.exit();
    })
}


module.exports = {
    logs_post_task:logs_post_task
}