var dutils = require('./downloadUtils.js');
var db = require('./dbService.js');
var fs = require('fs');

var state = 'INIT';
var new_signature= null;
var schedule_url = null;
var start_signature = 0;
var schedule_array = [];
var sh_array_temp = null;
var schedule_file_global = null;
var schedule_string = null;

//download schedule
function schedule_download_task()
{
    db.get_config_param('schedule_url')
    .then(sch_url=>{
        schedule_url = sch_url;
        return dutils.request_get(sch_url);
    })
    .then((res)=>{
        res = dutils.decrypt(res.body);
        console.log("OK");
        schedule_string=res;
        start_signature = res.indexOf("#BEGIN#");
        var stop_signature = res.indexOf("#END#");
        new_signature = res.substring(start_signature+7,stop_signature);
    })
    .then(()=>db.get_config_param('schedule_signature'))
    .then(actual_sig=>{
        if(actual_sig==new_signature)
        {
                    
            db.log('schedule','schedule is actual. Terminating',"info");
            process.exit()
        }
    })
   .then(()=>dutils.save_schedule(schedule_string,'tmp_schedule.json.crypto'))
   .then(()=>dutils.verify_file('tmp_schedule.json.crypto'))
   .then((res)=>{
        return new Promise(function(resolve,reject){
            db.log('schedule','new schedule verified. Try to parse content',"success");
            fs.readFile('tmp_schedule.json.crypto',function(error,data){
                if(error)
                {
                    db.log('schedule','Cannot parse schedule','error');
                    reject();
                }
                else
                {
                    data = data.toString();
                    var schedule_file = JSON.parse(data.slice(0,start_signature));  
                    if(typeof schedule_file=="object")
                    {
                        schedule_file_global = schedule_file;  
                        resolve(schedule_file);
                    } 
                    else
                    {
                            db.log('schedule','Cannot parse schedule',"error");
                            reject();
                    }
                }
            })
        })

   })
   .then(()=>dutils.get_md5_from_folder('videos'))
   .then(function(result_array){

        schedule_array=schedule_file_global.schedule;
        return dutils.prepare_arrays(schedule_array,result_array)
    })
    .then(function(sh_array){
        sh_array_temp=sh_array;
        if(sh_array.download.length>0)
        {
            db.log("schedule","downloading files","info");
            return dutils.download_list(sh_array.download);
        }
        else
        {
            return new Promise(function(resolve,reject){
                db.log("schedule","download list empty","info");
                resolve();
            })
        }
     })
     .then(function(){
        if(sh_array_temp.delete.length>0)
        {
            db.log("schedule","deleting files","info");
            return dutils.delete_list(sh_array_temp.delete);
        }
        else
        {
            return new Promise(function(resolve,reject){
                db.log("schedule","delete list empty","info");
                resolve();
            })
        }
     })
     .then(function(){
         return new Promise(function(resolve,reject){
            var good_files = [];
            dutils.get_md5_from_folder('videos')
            .then(function(md5_list){
                   for(var i=0;i<schedule_array.length;i++)
                   {
                       for(var k=0;k<md5_list.length;k++)
                       {
                            if(schedule_array[i].md5==md5_list[k] || schedule_array[i].download_content==false)
                            {
                                good_files.push(schedule_array[i]);
                                break;
                            }
                       }
                   }
               resolve(good_files);

            })
         });
     })
     .then(function(g_files){
        if(g_files.length<schedule_array.length)
        {
            db.log("schedule","wrong files detected.","warning");
     
        }
        else
        {
            db.log("schedule","all files are good.","success");
            //set files to db
        }
        for(var i=0;i<g_files.length;i++)
        {
            if(g_files[i].type=="browser")
            {
                g_files[i]['local_path']=g_files[i].video_url;
            }
            else
            {
                g_files[i]['local_path']='videos/'+g_files[i].md5;
            }
            
        }
        db.set_schedule(g_files)
        .then(function(res){
            db.set_config_param('schedule_signature',new_signature)
            .then(function(){
                db.log("schedule","schedule updated. Terminating","success");
                process.exit();
            })
        })
    })
   .catch(function(error){
        db.log("schedule","Error During update. Terminating:::"+error,"error");
        process.exit();
    })
        
}


module.exports = {
    schedule_download_task:schedule_download_task
}