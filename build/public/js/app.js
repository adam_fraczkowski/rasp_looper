var url = '.'
var app = angular.module('rasp_looper',[]);

app.factory('Log',[function(){
    var latest = 3;
    return {
        log_array:[],
        log:function(text)
        {
            var log_array = this.log_array;
            if(log_array.length>=latest)
            {
                log_array.shift();
                log_array.push(moment().format('DD-MM-YYYY HH:mm:ss')+"#"+text);
            }
            else
            {
                log_array.push(moment().format('DD-MM-YYYY HH:mm:ss')+"#"+text);
            }
        },
        show:function()
        {
            var log_array = this.log_array;
            var log_text = "";
            for(var i=0;i<log_array.length;i++)
            {
                log_text = log_text + "::"+log_array[i]+'<br/>';
            }
            return log_text
        }
    }
}])

app.factory('Fetch',['$http',function($http){
    return {
        
        fetchConfig:function(login,pass)
        {
            return new Promise(function(resolve,reject){
                $http.get(url+'/get_config').then(function(res){
                    resolve(res);
                })
                .catch(function(error){
                    reject(error);
                })
            })
            
        },

        changeConfig:function(param,value)
        {
            return new Promise(function(resolve,reject){
                $http.post(url+'/set_config_param',{param:param,value:value})
                .then(function(res){
                    resolve(res);
                })
                .catch(function(error){
                    reject(error);
                })
            })
        },

        fetchSchedule:function()
        {
            return new Promise(function(resolve,reject){
                $http.get(url+'/get_schedule')
                .then(function(res){
                    resolve(res);
                })
                .catch(function(error){
                    reject(error);
                })
            })
        },

        fetchLogs:function(limit,offset)
        {
            return new Promise(function(resolve,reject){
                $http.get(url+'/get_logs?limit='+limit+'&offset='+offset)
                .then(function(res){
                    resolve(res);
                })
                .catch(function(error){
                    reject(error);
                })
            })
        }

    }
}])

app.controller('FetchCtrl',['Fetch','Log','$scope',function(Fetch,Log,$scope){
    $scope.config_data = {};
    $scope.config_val = {};

    $scope.schedule_data = {};

    $scope.schedule_logs = [];

    $scope.actual_logs_page = 1;

    $scope.init = function()
    {
        Fetch.fetchConfig()
        .then(function(result){
            Log.log("FETCH CONFIG DATA");
            $scope.notify = Log.log_array;
            delete result.data['schedule_signature'];
            delete result.data['update_signature'];
            delete result.data['config_signature'];
            if(!result.data["summary"]) $scope.summary_data = result.data["summary"];
            else $scope.summary_data = JSON.parse(result.data["summary"]);
            delete result.data['summary'];
            $scope.config_data = result.data;
            $scope.$apply();
        })
        .catch(function(error){
            Log.log("ERROR DURING FETCH CONFIG DATA");
            console.log(error);
            $scope.notify = Log.log_array;
            $scope.$apply();
        })

        Fetch.fetchSchedule()
        .then(function(result){
            Log.log("FETCH SCHEDULE DATA");
            $scope.notify = Log.log_array;
            $scope.schedule_data = result.data;
            $scope.$apply();
        })
        .catch(function(error){
            Log.log("ERROR DURING FETCH SCHEDULE DATA");
            $scope.notify = Log.log_array;
            $scope.$apply();
        })

        Fetch.fetchLogs(10,0)
        .then(function(result){
            Log.log("FETCH LOGS DATA");
            $scope.notify = Log.log_array;
            $scope.logs_data = result.data;
            $scope.$apply();
        })
        .catch(function(error){
            Log.log("ERROR DURING FETCH LOGS DATA");
            $scope.notify = Log.log_array;
            $scope.$apply();
        })
    }

    $scope.changeConfig = function(key)
    {
        Fetch.changeConfig(key,$scope.config_val[key])
        .then(function(res){
            Log.log("CONFIG CHANGED");
            $scope.notify = Log.log_array;
            $scope.$apply();
            $scope.init();
        })
        .catch(function(error){
            Log.log("ERROR DURING CHANGE CONFIG");
            $scope.notify = Log.log_array;
            $scope.$apply();
            $scope.init();
        })
    }

    $scope.changeLogsPage = function(page_number)
    {
        var limit = 10;
        var offset = ((parseInt(page_number)-1)*limit);
        if(page_number<1)
        {
            page_number=1;
        }
        $scope.actual_logs_page = page_number;
        Fetch.fetchLogs(limit,offset)
        .then(function(result){
            Log.log("FETCH LOGS DATA");
            $scope.notify = Log.log_array;
            $scope.logs_data = result.data;
            $scope.$apply();
        })
        .catch(function(error){
            Log.log("ERROR DURING FETCH LOGS DATA");
            $scope.notify = Log.log_array;
            $scope.$apply();
        })
    }

}]);