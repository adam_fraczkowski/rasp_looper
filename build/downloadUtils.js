var request = require('request');
var fs = require('fs');
var crypto = require('crypto');
var wget = require('wget-improved');
var path = require('path');
var public_key_path = path.join(__dirname, 'rasp_public.pem')

function request_get(url)
{
    return new Promise(function(resolve,reject){
        request.get(url,{},function(error,response){
            if(error)
            {
                reject("Error during REQUEST_GET schedule");
            }
            else
            {
                resolve(response);
            }
        })
    })
}

function calc_checksum(path)
{
    return new Promise(function(resolve,reject){
        var hash = crypto.createHash('md5');
        var stream = fs.createReadStream(path);
    
        stream.on('data',function(data){
            hash.update(data,'utf8');
        })
    
        stream.on('end',function(){
            resolve(hash.digest('hex'));
        })
    });
}

function download_update(source)
{
    return new Promise(function(resolve,reject){
        var filename = crypto.randomBytes(8).toString('hex');
        var file = wget.download(source,filename);
        file.on('end',function(output){
            calc_checksum(filename)
            .then(function(hash){
                fs.renameSync(filename,hash);
                resolve(hash);
            });
        })

        file.on('error',function(error){
            if(fs.existsSync(filename)) fs.unlink(filename);
            reject("Error during update download");
        });
    });
}


function download_file(source)
{
    return new Promise(function(resolve,reject){
        var filename = crypto.randomBytes(8).toString('hex');
        var file = wget.download(source,filename);
        file.on('end',function(output){
            calc_checksum(filename)
            .then(function(hash){
                fs.renameSync(filename,'videos/'+hash);
                resolve();
            });
        })

        file.on('error',function(error){
            if(fs.existsSync(filename)) fs.unlink(filename);
            resolve();
        });
    });
}

function download_schedule(source,dest)
{
    return new Promise(function(resolve,reject){
        var filename = dest;
        var file = wget.download(source,filename);
        
        file.on('end',function(output){
            resolve();            
        })

        file.on('error',function(error){
            if(fs.existsSync(filename)) fs.unlink(filename);
            resolve();
        });
    });
}



function verify_file(filename)
{
    return new Promise(function(resolve,reject){

        try
        {    
            var pub_key = fs.readFileSync(public_key_path);
            var file = fs.readFileSync(filename).toString();

            var start_signature = file.indexOf("#BEGIN#");
            var stop_signature = file.indexOf("#END#");

            var signature = file.substring(start_signature+7,stop_signature);

            var schedule_file = file.slice(0,start_signature);

            var verify = crypto.createVerify('sha256');
            verify.write(schedule_file);
            verify.end();

            if(verify.verify(pub_key,signature,'base64')==true)
            {
                resolve(true);
            }
            else
            {
                reject("Error During file verification ");
            }
        }
        catch(e)
        {
            reject("Error during file verification");
        }
    })
}

function verify_update(filename,signature)
{
    return new Promise(function(resolve,reject){

        try
        {    
            var pub_key = fs.readFileSync(public_key_path);
            var file = fs.readFileSync(filename);

            var verify = crypto.createVerify('sha256');
            verify.write(file);
            verify.end();

            if(verify.verify(pub_key,signature,'base64')==true)
            {
                resolve(filename);
            }
            else
            {
                reject(false);
            }
        }
        catch(e)
        {
            reject("Error During update verification");
        }
    })
}

function get_md5_from_folder(folder_name)
{
    var promise_array = [];
    var file_list = fs.readdirSync(folder_name);
    for(var i=0;i<file_list.length;i++)
    {
        promise_array.push(calc_checksum(folder_name+'/'+file_list[i]));
    }
    return Promise.all(promise_array);
}

function download_list(download_list)
{
    var list = [];
    for(var i=0;i<download_list.length;i++)
    {
        list.push(download_file(download_list[i].video_url,'videos/'+download_list[i].md5));
    }
    return Promise.all(list);

}

function delete_file(path)
{
    return new Promise(function(resolve,reject){
        fs.unlink(path,function(error,data){
            if(error)
            {
                reject("Error during file delete");
            }
            else
            {
                resolve();
            }
        })
    })
}

function delete_list(delete_list)
{
    var list =[];
    get_md5_from_folder('videos')
    .then(list)
    for(var i=0;i<delete_list.length;i++)
    {
        list.push(delete_file('videos/'+delete_list[i]));
    }
    return Promise.all(list);
}

function prepare_arrays(schedule_array,result_array)
{
    var download_array = [];
    var delete_array = [];
    var flag=0;
    return new Promise(function(resolve,reject){
        for(var i=0;i<schedule_array.length;i++)
        {
            flag=0;
            for(var k=0;k<result_array.length;k++)
            {
                if(schedule_array[i].md5==result_array[k])
                {
                    flag=1;
                }

                
            }
            if(flag==0)
            {
                if(schedule_array[i].download_content==true)
                {
                    download_array.push(schedule_array[i]);
                }
            }
            
        }
        
        for(var j=0;j<result_array.length;j++)
        {
            flag=0;
            for(var w=0;w<schedule_array.length;w++)
            {
                if(schedule_array[w].md5==result_array[j])
                {
                    flag=1;
                }
                
                
            }
            if(flag==0)
            {
                delete_array.push(result_array[j]);

            }
            
        }
        if(i==schedule_array.length && j==result_array.length)
        {
            resolve({download:download_array,delete:delete_array});
        }
        
    });
}

function encrypt(text)
{ 
   let algorithm = 'aes-256-ctr';
   let password = 'DhY67HGyt67e';
   let crypt = crypto.createCipher(algorithm,password);
   var crypted = crypt.update(text,'utf8','hex')
   crypted += crypt.final('hex');
   return crypted;
}


function decrypt(text)
{
   let algorithm = 'aes-256-ctr';
   let password = 'DhY67HGyt67e';
   let decipher = crypto.createDecipher(algorithm,password)
   let dec = decipher.update(text,'hex','utf8')
   dec += decipher.final('utf8');
   return dec;
}

function save_schedule(text,destination)
{
    return new Promise(function(resolve,reject){
        fs.writeFile(destination,text,function(error,resp){
            if(error)
            {
                reject("Error during save schedule file. Terminating");
            }
            else
            {
                resolve();
            }
        })
    })
}


module.exports = {
    download_file:download_file,
    calc_checksum:calc_checksum,
    request_get:request_get,
    verify_file:verify_file,
    get_md5_from_folder:get_md5_from_folder,
    delete_list:delete_list,
    download_list:download_list,
    prepare_arrays:prepare_arrays,
    download_schedule:download_schedule,
    encrypt:encrypt,
    decrypt:decrypt,
    download_update:download_update,
    verify_update:verify_update,
    save_schedule:save_schedule
}