#!bin/sh
ps aux | grep "./app" | grep -v grep > /dev/null
if [ $? != 0 ]
then
    export set DISPLAY=:0        
    ./app > /dev/null
fi
